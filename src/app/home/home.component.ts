import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../core/firebase.service';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private firebaseService: FirebaseService) { }

  ngOnInit() {
  }

  get() {
    notify('GET');
  }

  put() {
    notify('PUT');
  }

  post() {
    notify('POST');
    const movie = {
      title: 'Lion King',
      runtime: 120,
      year: 1994
    };

    this.firebaseService.items.push(movie);
  }

  delete() {
    notify('DELETE');
  }

}
