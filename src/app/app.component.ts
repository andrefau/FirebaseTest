import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from './core/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public isLoggedIn: boolean;

    constructor(public firebaseService: FirebaseService, private router: Router) {
      this.firebaseService.user.subscribe(
        (auth) => {
          if (auth == null) {
            console.log('Not Logged in.');
            this.router.navigate(['login']);
            this.isLoggedIn = false;
          } else {
            console.log('Successfully Logged in.');
            this.isLoggedIn = true;
            this.router.navigate(['']);
          }
        }
      );
  }

  logout() {
    this.firebaseService.logout();
  }
}
